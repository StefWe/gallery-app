# Chinese (Traditional) translation for gallery-app
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the gallery-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: gallery-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-11-22 21:06+0000\n"
"PO-Revision-Date: 2019-05-03 21:02+0000\n"
"Last-Translator: louies0623 <louies0623@gmail.com>\n"
"Language-Team: Chinese (Traditional) <https://translate.ubports.com/projects/"
"ubports/gallery-app/zh_Hant/>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 3.6.1\n"
"X-Launchpad-Export-Date: 2017-04-05 07:16+0000\n"

#: rc/qml/AlbumEditor/AlbumEditMenu.qml:43
#: rc/qml/AlbumEditor/AlbumEditor.qml:36
msgid "Edit album"
msgstr "編輯相簿"

#: rc/qml/AlbumEditor/AlbumEditMenu.qml:50
#: rc/qml/AlbumViewer/AlbumViewer.qml:378 rc/qml/Components/DeleteDialog.qml:32
#: rc/qml/EventsOverview.qml:139 rc/qml/MediaViewer/MediaViewer.qml:350
#: rc/qml/PhotosOverview.qml:195 rc/qml/Utility/EditingHUD.qml:66
msgid "Delete"
msgstr "刪除"

#: rc/qml/AlbumEditor/AlbumEditor.qml:39 rc/qml/Components/MediaSelector.qml:93
#: rc/qml/EventsOverview.qml:161 rc/qml/MediaViewer/MediaViewer.qml:278
#: rc/qml/PhotosOverview.qml:218 rc/qml/PickerScreen.qml:285
msgid "Cancel"
msgstr "取消"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:113
msgid "Default"
msgstr "預設"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:114
msgid "Blue"
msgstr "藍"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:115
msgid "Green"
msgstr "綠"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:116
msgid "Pattern"
msgstr "樣式"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:117
msgid "Red"
msgstr "紅"

#: rc/qml/AlbumViewer/AlbumViewer.qml:107 rc/qml/MainScreen.qml:62
msgid "Album"
msgstr "專輯"

#: rc/qml/AlbumViewer/AlbumViewer.qml:372
#: rc/qml/MediaViewer/MediaViewer.qml:340
msgid "Add to album"
msgstr "加入相簿"

#: rc/qml/AlbumsOverview.qml:229 rc/qml/Components/PopupAlbumPicker.qml:91
msgid "Add new album"
msgstr "加入新的相簿"

#: rc/qml/AlbumsOverview.qml:233 rc/qml/Components/PopupAlbumPicker.qml:101
msgid "New Photo Album"
msgstr "新的相片相簿"

#: rc/qml/AlbumsOverview.qml:234 rc/qml/Components/PopupAlbumPicker.qml:102
msgid "Subtitle"
msgstr "字幕"

#: rc/qml/AlbumsOverview.qml:241 rc/qml/EventsOverview.qml:116
#: rc/qml/PhotosOverview.qml:173
msgid "Camera"
msgstr "相機"

#: rc/qml/Components/DeleteDialog.qml:36 rc/qml/MediaViewer/MediaViewer.qml:226
msgid "Yes"
msgstr "是"

#: rc/qml/Components/DeleteDialog.qml:45 rc/qml/MediaViewer/MediaViewer.qml:237
msgid "No"
msgstr "否"

#: rc/qml/Components/DeleteOrDeleteWithContentsDialog.qml:51
msgid "Delete album"
msgstr "刪除相簿"

#: rc/qml/Components/DeleteOrDeleteWithContentsDialog.qml:60
msgid "Delete album AND contents"
msgstr "刪除相簿及其內容"

#: rc/qml/Components/EventCard.qml:59
msgid "MMM yyyy"
msgstr "yyyy MMM"

#: rc/qml/Components/EventCard.qml:73
msgid "dd"
msgstr "dd"

#: rc/qml/Components/MediaSelector.qml:59
#: rc/qml/Components/MediaSelector.qml:81
msgid "Add to Album"
msgstr "加入到相簿"

#: rc/qml/Components/PopupAlbumPicker.qml:162 rc/qml/Utility/EditingHUD.qml:77
msgid "Add Photo to Album"
msgstr "將相片加入到相簿"

#: rc/qml/Components/UnableShareDialog.qml:25
msgid "Unable to share"
msgstr "無法分享"

#: rc/qml/Components/UnableShareDialog.qml:26
msgid "Unable to share photos and videos at the same time"
msgstr "無法同時分享相片與影片"

#: rc/qml/Components/UnableShareDialog.qml:30
msgid "Ok"
msgstr "確定"

#: rc/qml/EventsOverview.qml:80 rc/qml/PhotosOverview.qml:87
#, qt-format
msgid "Delete %1 photo"
msgid_plural "Delete %1 photos"
msgstr[0] "刪除 %1 張相片"

#: rc/qml/EventsOverview.qml:86 rc/qml/PhotosOverview.qml:93
#, qt-format
msgid "Delete %1 video"
msgid_plural "Delete %1 videos"
msgstr[0] "刪除 %1 份影片"

#: rc/qml/EventsOverview.qml:92 rc/qml/PhotosOverview.qml:99
#, qt-format
msgid "Delete %1 media file"
msgid_plural "Delete %1 media files"
msgstr[0] "刪除 %1 份媒體檔案"

#: rc/qml/EventsOverview.qml:109 rc/qml/MainScreen.qml:199
#: rc/qml/MainScreen.qml:251 rc/qml/PhotosOverview.qml:166
#: rc/qml/PickerScreen.qml:192 rc/qml/PickerScreen.qml:239
msgid "Select"
msgstr "選取"

#: rc/qml/EventsOverview.qml:128 rc/qml/PhotosOverview.qml:185
#: rc/qml/Utility/EditingHUD.qml:76
msgid "Add"
msgstr "加入"

#: rc/qml/EventsOverview.qml:146 rc/qml/MediaViewer/MediaViewer.qml:361
#: rc/qml/PhotosOverview.qml:202 rc/qml/Utility/EditingHUD.qml:71
msgid "Share"
msgstr "分享"

#: rc/qml/EventsOverview.qml:195 rc/qml/MediaViewer/MediaViewer.qml:182
#: rc/qml/PhotosOverview.qml:241
msgid "Share to"
msgstr "分享到"

#: rc/qml/LoadingScreen.qml:39
msgid "Loading…"
msgstr "載入中…"

#: rc/qml/MainScreen.qml:148
msgid "Albums"
msgstr "專輯"

#: rc/qml/MainScreen.qml:162 rc/qml/MainScreen.qml:201
#: rc/qml/PickerScreen.qml:154
msgid "Events"
msgstr "事件"

#: rc/qml/MainScreen.qml:210 rc/qml/MainScreen.qml:253
#: rc/qml/PickerScreen.qml:203
msgid "Photos"
msgstr "相片"

#: rc/qml/MediaViewer/MediaViewer.qml:215
msgid "Delete a photo"
msgstr "刪除一張相片"

#: rc/qml/MediaViewer/MediaViewer.qml:215
msgid "Delete a video"
msgstr "刪除一份影片"

#: rc/qml/MediaViewer/MediaViewer.qml:248
msgid "Remove a photo from album"
msgstr "從相簿移除一張相片"

#: rc/qml/MediaViewer/MediaViewer.qml:248
msgid "Remove a video from album"
msgstr "從相簿中移除一份影片"

#: rc/qml/MediaViewer/MediaViewer.qml:257
msgid "Remove from Album"
msgstr "從相簿中移除"

#: rc/qml/MediaViewer/MediaViewer.qml:268
msgid "Remove from Album and Delete"
msgstr "從相簿中移除並刪除"

#: rc/qml/MediaViewer/MediaViewer.qml:315
msgid "Edit"
msgstr "編輯"

#: rc/qml/MediaViewer/MediaViewer.qml:371
msgid "Info"
msgstr "資訊"

#: rc/qml/MediaViewer/MediaViewer.qml:394
msgid "Informations"
msgstr "訊息"

#: rc/qml/MediaViewer/MediaViewer.qml:397
msgid "Media type: "
msgstr "媒體類型： "

#: rc/qml/MediaViewer/MediaViewer.qml:398
msgid "photo"
msgstr "相片"

#: rc/qml/MediaViewer/MediaViewer.qml:398
msgid "video"
msgstr "影片"

#: rc/qml/MediaViewer/MediaViewer.qml:399
msgid "Media name: "
msgstr "媒體名稱： "

#: rc/qml/MediaViewer/MediaViewer.qml:400
msgid "Date: "
msgstr "日期： "

#: rc/qml/MediaViewer/MediaViewer.qml:401
msgid "Time: "
msgstr "時間： "

#: rc/qml/MediaViewer/MediaViewer.qml:407
msgid "ok"
msgstr "確定"

#: rc/qml/MediaViewer/PhotoEditorPage.qml:26
msgid "Edit Photo"
msgstr "編輯相片"

#: rc/qml/MediaViewer/PopupPhotoViewer.qml:46
#: desktop/gallery-app.desktop.in.in.h:1
msgid "Gallery"
msgstr "相簿"

#: rc/qml/MediaViewer/PopupPhotoViewer.qml:183
msgid "Toggle Selection"
msgstr "切換選取項目"

#: rc/qml/MediaViewer/SingleMediaViewer.qml:241
msgid "An error has occurred attempting to load media"
msgstr "於嘗試載入媒體時發生錯誤"

#: rc/qml/PhotosOverview.qml:116 rc/qml/PhotosOverview.qml:159
msgid "Grid Size"
msgstr "網格尺寸大小"

#: rc/qml/PhotosOverview.qml:117
msgid "Select the grid size in gu units between 8 and 20 (default is 12)"
msgstr "以 gu 單位為單位選擇8到20之間的網格尺寸大小 (預設值為 12)"

#: rc/qml/PhotosOverview.qml:132
msgid "Finished"
msgstr "完成"

#: rc/qml/PickerScreen.qml:291
msgid "Pick"
msgstr "選擇"

#: rc/qml/Utility/EditingHUD.qml:67
msgid "Trash;Erase"
msgstr "垃圾;清除"

#: rc/qml/Utility/EditingHUD.qml:72
msgid "Post;Upload;Attach"
msgstr "張貼;上傳;附件"

#: rc/qml/Utility/EditingHUD.qml:81
msgid "Undo"
msgstr "復原"

#: rc/qml/Utility/EditingHUD.qml:82
msgid "Cancel Action;Backstep"
msgstr "取消動作;上一步"

#: rc/qml/Utility/EditingHUD.qml:86
msgid "Redo"
msgstr "重做"

#: rc/qml/Utility/EditingHUD.qml:87
msgid "Reapply;Make Again"
msgstr "重新套用;重新再做"

#: rc/qml/Utility/EditingHUD.qml:91
msgid "Auto Enhance"
msgstr "自動補強"

#: rc/qml/Utility/EditingHUD.qml:92
msgid "Adjust the image automatically"
msgstr "自動調整影像"

#: rc/qml/Utility/EditingHUD.qml:93
msgid "Automatically Adjust Photo"
msgstr "自動調整相片"

#: rc/qml/Utility/EditingHUD.qml:98
msgid "Rotate"
msgstr "旋轉"

#: rc/qml/Utility/EditingHUD.qml:99
msgid "Turn Clockwise"
msgstr "順時針旋轉"

#: rc/qml/Utility/EditingHUD.qml:100
msgid "Rotate the image clockwise"
msgstr "順時針旋轉影像"

#: rc/qml/Utility/EditingHUD.qml:105
msgid "Crop"
msgstr "裁切"

#: rc/qml/Utility/EditingHUD.qml:106
msgid "Trim;Cut"
msgstr "裁剪;剪下"

#: rc/qml/Utility/EditingHUD.qml:107
msgid "Crop the image"
msgstr "裁切影像"

#: rc/qml/Utility/EditingHUD.qml:112
msgid "Revert to Original"
msgstr "還原至初始狀態"

#: rc/qml/Utility/EditingHUD.qml:113
msgid "Discard Changes"
msgstr "放棄修改"

#: rc/qml/Utility/EditingHUD.qml:114
msgid "Discard all changes"
msgstr "放棄所有修改"

#: rc/qml/Utility/EditingHUD.qml:118
msgid "Exposure"
msgstr "曝光"

#: rc/qml/Utility/EditingHUD.qml:119
msgid "Adjust the exposure"
msgstr "調整曝光"

#: rc/qml/Utility/EditingHUD.qml:120
msgid "Underexposed;Overexposed"
msgstr "曝光不足;曝光過度"

#: rc/qml/Utility/EditingHUD.qml:121 rc/qml/Utility/EditingHUD.qml:162
msgid "Confirm"
msgstr "確認"

#: rc/qml/Utility/EditingHUD.qml:125
msgid "Compensation"
msgstr "曝光補償"

#: rc/qml/Utility/EditingHUD.qml:159
msgid "Color Balance"
msgstr "色彩平衡"

#: rc/qml/Utility/EditingHUD.qml:160
msgid "Adjust color balance"
msgstr "調整色彩平衡"

#: rc/qml/Utility/EditingHUD.qml:161
msgid "Saturation;Hue"
msgstr "飽和度;色調"

#: rc/qml/Utility/EditingHUD.qml:166
msgid "Brightness"
msgstr "亮度"

#: rc/qml/Utility/EditingHUD.qml:177
msgid "Contrast"
msgstr "對比"

#: rc/qml/Utility/EditingHUD.qml:188
msgid "Saturation"
msgstr "飽和度"

#: rc/qml/Utility/EditingHUD.qml:199
msgid "Hue"
msgstr "色調"

#: desktop/gallery-app.desktop.in.in.h:2
msgid "Ubuntu Photo Viewer"
msgstr "Ubuntu 相片檢視器"

#: desktop/gallery-app.desktop.in.in.h:3
msgid "Browse your photographs"
msgstr "瀏覽您的相片"

#: desktop/gallery-app.desktop.in.in.h:4
msgid "Photographs;Pictures;Albums"
msgstr "相片;圖片;相簿"

#~ msgid "Delete 1 photo"
#~ msgstr "刪除 1 張相片"

#~ msgid "Delete 1 video"
#~ msgstr "刪除 1 份影片"

#~ msgid "Delete %1 photos and 1 video"
#~ msgstr "刪除 %1 張相片與 1 份影片"

#~ msgid "Delete 1 photo and %1 videos"
#~ msgstr "刪除 1 張相片與 %1 份影片"

#~ msgid "Delete 1 photo and 1 video"
#~ msgstr "刪除 1 張相片與 1 份影片"

#~ msgid "Delete %1 photos and %2 videos"
#~ msgstr "刪除 %1 張相片與 %2 份影片"
